package com.brejaclub.authenticated.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel

open class HomeViewModel(val app: Application) : AndroidViewModel(app) {

}