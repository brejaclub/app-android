package com.brejaclub.authenticated.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel

open class SettingsViewModel(val app: Application) : AndroidViewModel(app) {
}