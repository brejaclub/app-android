package com.brejaclub.unauthenticated.domain

data class User(var email: String = "", var password: String = "", var name: String = "", var confirmPassword: String = "")