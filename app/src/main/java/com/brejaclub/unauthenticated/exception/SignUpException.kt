package com.brejaclub.unauthenticated.exception

class LoginException(val code: Int, override val message: String?): Exception(message)
class SignUpException(val code: Int, override val message: String?): Exception(message)
