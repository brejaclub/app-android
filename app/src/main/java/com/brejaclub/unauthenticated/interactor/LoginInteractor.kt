package com.brejaclub.unauthenticated.interactor

import com.brejaclub.unauthenticated.domain.User
import com.brejaclub.unauthenticated.repository.LoginRepository
import com.brejaclub.unauthenticated.repository.dto.UserDTO

open class LoginInteractor {

    val repository = LoginRepository()

    fun login(user: User, callback: (error: Exception?) -> Unit) {

        val userDTO = UserDTO(user.email, user.password)
        repository.login(userDTO, callback)
    }
}