package com.brejaclub.unauthenticated.interactor

import com.brejaclub.unauthenticated.domain.User
import com.brejaclub.unauthenticated.repository.RecoverPasswordRepository
import java.lang.Exception

class RecoverPasswordInteractor {

    private val repository = RecoverPasswordRepository()

    fun recoverPassoword(user: User?, callback: (error: Exception?) -> Unit) {
        repository.recoverPassword(user?.email.toString(), callback)
    }
}