package com.brejaclub.unauthenticated.interactor

import com.brejaclub.unauthenticated.domain.User
import com.brejaclub.unauthenticated.repository.RegisterRepository
import com.brejaclub.unauthenticated.repository.dto.UserDTO

class RegisterInteractor {

    val repository = RegisterRepository()

    fun signUp(user: User?, callback: (error: Exception?) -> Unit) {

        val userDTO = UserDTO(user?.email.toString(), user?.password.toString())
        repository.signUp(userDTO, callback)
    }

}