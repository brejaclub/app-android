package com.brejaclub.unauthenticated.repository

import com.brejaclub.unauthenticated.repository.dto.UserDTO
import com.google.firebase.auth.FirebaseAuth

open class LoginRepository {

    val auth = FirebaseAuth.getInstance()

    fun login(user: UserDTO, callback: (error: Exception?) -> Unit) {

        val task = auth.signInWithEmailAndPassword(user.email, user.password)
        task.addOnCompleteListener {
            callback(null ?: it.exception)
        }
    }

}
