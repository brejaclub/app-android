package com.brejaclub.unauthenticated.repository

import com.google.firebase.auth.FirebaseAuth

class RecoverPasswordRepository {
    val auth = FirebaseAuth.getInstance()

    fun recoverPassword(email: String, callback: (error: Exception?) -> Unit) {

        val task = auth.sendPasswordResetEmail(email)
        task.addOnCompleteListener {
            callback(null ?: it.exception)
        }
    }
}