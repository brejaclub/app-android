package com.brejaclub.unauthenticated.repository

import com.brejaclub.unauthenticated.repository.dto.UserDTO
import com.google.firebase.auth.FirebaseAuth

open class RegisterRepository {

    fun signUp(user: UserDTO, callback: (error: Exception?) -> Unit) {
        val auth = FirebaseAuth.getInstance()

        val task = auth.createUserWithEmailAndPassword(user.email, user.password)
        task.addOnCompleteListener {
            callback(null ?: it.exception)
//            if (result.isSuccessful) {
//                callback(null)
//            } else {
//                //TODO: Implementar as variações de código
//                callback(SignUpException(-9998, result.exception?.localizedMessage))
//            }
        }
    }
}