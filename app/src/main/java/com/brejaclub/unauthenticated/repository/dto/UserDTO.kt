package com.brejaclub.unauthenticated.repository.dto

data class UserDTO(val email: String = "", val password: String = "", var token: String? = "")