package com.brejaclub.unauthenticated.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.brejaclub.R

class UnAuthenticatedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.supportActionBar?.hide();
        setContentView(R.layout.activity_unauthenticated)
    }
}