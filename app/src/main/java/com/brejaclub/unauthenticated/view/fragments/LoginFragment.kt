package com.brejaclub.unauthenticated.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.brejaclub.R
import com.brejaclub.databinding.FragmentLoginBinding
import com.brejaclub.unauthenticated.viewmodel.LoginViewModel

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding

    private val viewModel: LoginViewModel by lazy {
        ViewModelProvider(this).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.fragment = this
        binding.lifecycleOwner = this

        return binding.root
    }

    fun login(view: View) {

//        viewModel.makeLogin {
//            if (it == null) {
                findNavController().navigate(R.id.action_loginFragment_to_authenticatedActivity)
//            } else {
//                Toast.makeText(context, it.localizedMessage, Toast.LENGTH_LONG).show()
//            }
//        }
    }

    fun forgotPassword(view: View){
        findNavController().navigate(R.id.action_loginFragment_to_forgotPasswordFragment)
    }

    fun back(view: View) {
        findNavController().popBackStack()
    }
}

