package com.brejaclub.unauthenticated.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.brejaclub.R
import com.brejaclub.unauthenticated.viewmodel.RegisterViewModel
import com.brejaclub.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding

    private val viewModel: RegisterViewModel by lazy {
        ViewModelProvider(this).get(RegisterViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.fragment = this
        binding.lifecycleOwner = this

        return binding.root
    }

    fun register(view: View) {

        viewModel.register {
            if (it == null) {
                Toast.makeText(context, R.string.create_account_success_message, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, it.localizedMessage, Toast.LENGTH_LONG).show()
            }
        }
    }

    fun back(view: View){
        findNavController().popBackStack()
    }
}