package com.brejaclub.unauthenticated.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.brejaclub.R
import com.brejaclub.databinding.FragmentSplashBinding
import com.brejaclub.unauthenticated.viewmodel.SplashViewModel

class SplashFragment : Fragment() {
    private lateinit var binding: FragmentSplashBinding
    private val viewModel: SplashViewModel by lazy {
        ViewModelProvider(this).get(SplashViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSplashBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.fragment = this
        binding.lifecycleOwner = this

        return binding.root
    }

    fun goToLogin(view: View) {
        findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
    }

    fun goToRegister(view: View) {
        findNavController().navigate(R.id.action_splashFragment_to_registerFragment)
    }

}