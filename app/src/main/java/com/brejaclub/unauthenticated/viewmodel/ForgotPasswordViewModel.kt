package com.brejaclub.unauthenticated.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.brejaclub.unauthenticated.domain.User
import com.brejaclub.unauthenticated.interactor.RecoverPasswordInteractor

class ForgotPasswordViewModel(val app: Application) : AndroidViewModel(app) {

    private val interactor = RecoverPasswordInteractor()

    val user = MutableLiveData<User>()

    fun recoverPassword(callback: (error: Exception?) -> Unit) {
        interactor.recoverPassoword(user.value, callback)
    }

}