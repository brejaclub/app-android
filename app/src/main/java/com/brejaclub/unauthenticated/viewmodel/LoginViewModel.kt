package com.brejaclub.unauthenticated.viewmodel

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.brejaclub.R
import com.brejaclub.unauthenticated.interactor.LoginInteractor
import kotlin.Exception
import com.brejaclub.unauthenticated.domain.User

open class LoginViewModel(val app: Application) : AndroidViewModel(app) {

    private val interactor = LoginInteractor()

    var user = MutableLiveData<User>()

    fun makeLogin(callback: (error: Exception?) -> Unit) {
        //regras de apresentacao
        //remover mascaras, etc

        val email = user.value?.email.toString()
        val password = user.value?.password.toString()

        if (TextUtils.isEmpty(email)) {
            callback(Exception(app.getString(R.string.email_required)))
            return
        }

        if (TextUtils.isEmpty(password)) {
            callback(Exception(app.getString(R.string.password_required)))
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            callback(Exception(app.getString(R.string.email_not_valid)))
            return
        }

        val user = User(email, password)
        interactor.login(user, callback)
    }
}