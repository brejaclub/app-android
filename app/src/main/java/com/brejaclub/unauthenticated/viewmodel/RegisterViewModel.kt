package com.brejaclub.unauthenticated.viewmodel

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.brejaclub.R
import com.brejaclub.unauthenticated.domain.User
import com.brejaclub.unauthenticated.interactor.RegisterInteractor

class RegisterViewModel(val app: Application) : AndroidViewModel(app) {

    private val interactor = RegisterInteractor()

    var user = MutableLiveData<User>(User())

    fun register(callback: (error: Exception?) -> Unit) {

        if(TextUtils.isEmpty(user.value?.email)) {
            callback(Exception(app.getString(R.string.email_required)))
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(user.value?.email).matches()) {
            callback(Exception(app.getString(R.string.email_not_valid)))
            return
        }

        if(TextUtils.isEmpty(user.value?.email)) {
            callback(Exception(app.getString(R.string.password_required)))
        }

        interactor.signUp(user.value, callback)
    }
}