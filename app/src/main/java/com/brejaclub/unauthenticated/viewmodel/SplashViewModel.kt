package com.brejaclub.unauthenticated.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class SplashViewModel(val app: Application) : AndroidViewModel(app) {
}